import argparse
from typing import TextIO
from enum import Enum, auto
import heapq


class Transform(Enum):
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()


ALL_TRANSFORMS = [Transform.UP, Transform.DOWN, Transform.LEFT, Transform.RIGHT]

TRANSFORM_MAP = {
    Transform.UP: (-1, 0),
    Transform.DOWN: (1, 0),
    Transform.LEFT: (0, -1),
    Transform.RIGHT: (0, 1),
}


def transform(position, tf):
    row, column = position
    offset_row, offset_column = TRANSFORM_MAP[tf]
    return row + offset_row, column + offset_column


def is_valid(position, num_rows, num_cols):
    row, column = position
    return 0 <= row < num_rows and 0 <= column < num_cols


def multiply_grid(grid):
    num_rows = len(grid)
    num_cols = len(grid[0])

    new_grid = []
    for row_index in range(num_rows * 5):
        new_row = []
        for col_index in range(num_cols * 5):
            copied_row = row_index % num_rows
            copied_col = col_index % num_cols
            offset = row_index // num_rows + col_index // num_cols
            new_value = grid[copied_row][copied_col] + offset
            if new_value > 9:
                new_value -= 9
            new_row.append(new_value)
        new_grid.append(new_row)

    return new_grid


class Vertex:
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost
        self.neighbors = []

    def add_neighbor(self, name):
        self.neighbors.append(name)

    def get_neighbors(self):
        return self.neighbors

    def get_cost(self):
        return self.cost


class Graph:
    def __init__(self, text, multiply=False):
        grid = []
        for line in text.split('\n'):
            row = []
            for risk in line:
                row.append(int(risk))
            grid.append(row)
        if multiply:
            grid = multiply_grid(grid)

        self.num_rows = len(grid)
        self.num_cols = len(grid[0])

        self.vertexes = {}
        for row_index, row in enumerate(grid):
            for col_index, risk in enumerate(row):
                position = (row_index, col_index)
                name = f'{row_index},{col_index}'
                vertex = Vertex(name, risk)
                neighbors = [transform(position, t) for t in ALL_TRANSFORMS]
                neighbors = [n for n in neighbors if is_valid(n, self.num_rows, self.num_cols)]
                neighbors = [f'{n[0]},{n[1]}' for n in neighbors]
                for n in neighbors:
                    vertex.add_neighbor(n)
                self.vertexes[name] = vertex

        self.unvisited = [name for name in self.vertexes]
        self.current = None
        self.visited = []
        self.dijkstra_table = {name: {'cost': float('inf'), 'previous': None} for name in self.vertexes}
        self.cost_heap = []

        self.start = '0,0'
        self.end = f'{self.num_rows - 1},{self.num_cols - 1}'
        self.dijkstra_table[self.start]['cost'] = 0
        heapq.heappush(self.cost_heap, (0, self.start))

    def find_cheapest_path(self):
        while len(self.unvisited) != 0:
            print(len(self.unvisited))
            # self.current = min(self.unvisited, key=lambda x: self.dijkstra_table[x]['cost'])
            current_cost, self.current = heapq.heappop(self.cost_heap)
            current_vertex = self.vertexes[self.current]
            for neighbor in current_vertex.get_neighbors():
                cost = current_cost + self.vertexes[neighbor].get_cost()
                if cost < self.dijkstra_table[neighbor]['cost']:
                    self.dijkstra_table[neighbor]['cost'] = cost
                    self.dijkstra_table[neighbor]['previous'] = self.current
                    heapq.heappush(self.cost_heap, (cost, neighbor))
            self.unvisited.remove(self.current)
            self.visited.append(self.current)
            if self.current == self.end:
                break
        return self.dijkstra_table[self.end]['cost']


def part_1(input_file: TextIO):
    graph = Graph(input_file.read())

    return graph.find_cheapest_path()


def part_2(input_file: TextIO):
    graph = Graph(input_file.read(), multiply=True)

    return graph.find_cheapest_path()


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 15')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
